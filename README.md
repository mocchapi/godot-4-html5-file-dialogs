# HTML5 File Dialogs (for godot 4)
Godot 4 addon which adds a read-only native HTML5 filesystem dialog node `HTML5FileDialog` which can prompt the user to upload file(s) or (multiple) directories in web exports. An array of filters can also be provided which limits what a user can 


The node's signals return (arrays of) `HTML5FileHandle` objects which wrap around javascript File objects, which are used to read the contents of selected files.

`HTML5FileHandle`s are used as such:
```gdscript
func _on_HTML5FileDialog_files_selected(files:Array[HTML5FileHandle]):
	for file in files:
		print("Text contents of ",file.path,':')
		print(await file.as_text()) # Returns string
		print("Binary contents of ",file.path,':')
		print(await file.as_buffer()) # Returns PackedByteArray
		print()
```
(You can also connect to the `text_loaded` or `buffer_loaded` signal on the `HTML5FileHandle` instead of using `await`)


For obvious reasons this node only works in web exports.


_______


Note that if you prompt the user for one or more directories, the result will be an *array of files that are (recursively) present in that directory*, not some directory object. If you have to, you can discern what the most top-level directory name is by comparing what the shortest path is between all the returned `HTML5FileHandle`s.


Under the hood this uses invisible `<input type="file">` html5 elements. This should mean it works on all mainstream browsers (unlike the FileSystem api approach), but also means that the user needs to have some amount of interaction with the page before the browser lets you open a dialog. This usually means the user needs to have clicked something at some point since the page loaded or the last popup was dismissed, so it generally should not be an issue.


This addon was inspired by [HTML5 File Exchange for Godot 3.4](https://github.com/Pukkah/HTML5-File-Exchange-for-Godot)